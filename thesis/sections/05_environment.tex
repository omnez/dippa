Tässä luvussa kuvataan vaatimusmäärittelyn täyttävän toteutuksen peruskomponentit automaattiselle uusiokäyttöjärjestelmälle.
Luvussa käydään läpi teknisiä konsepteja joiden päälle järjestelmä rakentuu, jonka jälkeen esitellään toteutuksen rakenne yleisellä tasolla.
Tarkempi uusiokäyttöprosessi kuvataan luvussa 6.

Automatosoitu toteutus koostuu funktionaalisista komponenteista, joista jokaisella on hyvin määritelty alku- ja lopputulos.
Komponentit toteutetaan yksinkertaisina skripteinä tai ohjelmina ja niitä ajetaan funktionaalisesti automaatioputkissa.
Yksittäiset komponentit ottavat syötteenä aiemman komponentin lopputilan, tai lukevat lähtötietonsa jostain informaatiolähteestä.
Vastaavasti lopputuloksensa ne antavat eteenpäin seuraavalle komponentille, tai päivittävät jotain informaatiolähdettä.
Komponentista riippuen, seuraava komponentti saatetaan käynnistää automaattisesti tai vasta käyttäjän pyynnöstä.

\subsection{Automaatioputket}
Automaatioputki (engl. pipeline) on yläkäsite, jonka olennaisena osana on suora tuotantoonvienti koodikannan muutosten perusteella. 
Koodikannan muutokset eivät tosin ole ehdoton edellytys automaatioputkien käyttämiselle.
\cite{gitlab-pipeline}

Automaatioputkien perusajatus on hallita jollain keskitetyllä järjestelmällä yksiselitteisesti määriteltyjä tehtäviä, ja jakaa näiden tehtävien suorittaminen suoritusyksiköille.
Suoritusyksiköt voidaan toteuttaa usealla eri tavalla käyttötarkoituksesta riippuen, mutta yhdistävänä tekijänä niillä kaikilla on kyky suorittaa laskentaa automaatioputkien ohjeiden perusteella.
\cite{gitlab-runner}

Yksinkertainen automaatioputki perustuu YAML-merkintäkielellä toteutettuun tiedostoon, jossa automaatioputken vaiheet ja tehtävät määritellään.
Esimerkiksi gitlab toteuttaa tämän kuvassa \ref{gitlab-ci} osoitetulla tavalla.
\cite{gitlab-runner-example}

\begin{figure}[h]
    \caption{Esimerkki yksinkertaisesta automaatioputkesta YAML-formaatissa}
    \centering
    \vspace*{3mm}
    \begin{tabular}{ |l| }
        \hline
         \\
        \begin{minipage}{0.9\textwidth}
        \VerbatimInput{files/.gitlab-ci.yml}
        \end{minipage}\\
         \\
        \hline
    \end{tabular}
    \label{gitlab-ci}
\end{figure}

Kuvassa \ref{gitlab-ci} esitetty automaatioputki on suhteellisen yksinkertainen esimerkki, mutta siinä nähdään selkeästi kaikki automaatioputkien perusperiaatteet.
Riveillä 1, 6, 11 ja 17 määritellään jokainen tehtävä, joille määritellään vaiheet riveillä 2, 7, 12 ja 18.
Jokaisella tehtävällä on selkeästi määritelty skriptiosuus, jossa sen tarkempi määrittely sijaitsee.
Viimeisessä ja tehtävässä on määritelty lisäksi muuttujia ja lisäehtoja sen suorittamiselle, vaihe osaa automaattisesti määrittää kulloisenkin versionhallinnan haaran ja vaihe toteutetaan ainoastaan tuotantoympäristöön.
Myös ensimmäisessä tehtävässä tekstiä muokataan sen mukaan, mikä käyttäjä on vastuussa kulloisenkin automaatioputken käynnistämisestä.
Näiden määritelmien lisäksi jokaiselle vaiheelle on mahdollista määrittää suoritukseen käytettävä ohjelmistokontti ja suoritusyksikkö, jossa tehtävä suoritetaan.

Koska automaatioputket määritellään täysin tiedostopohjaisesti, on niiden versio- ja muutoshallinta helppoa.
Jokainen muutos voidaan palauttaa aiempaan versioon tarvittaessa, ja automaatioputkille voidaan asettaa lukuisia ehtoja ja rajoituksia sen suhteen, milloin ja miten niitä ajetaan.
Automaatioputket tarjoavat yksinkertaisen ja helpon alustan automatisoitujen operaatioiden suorittamiselle, sillä ne on usein toteutettu suoraan versionhallinnan yhteyteen.

Koska automaatioputket toteutetaan usein suoraan koodikannan yhteyteen, voidaan niitä käyttää esimerkiksi suorittamaan automaattisia testejä ja tuotantoonvientejä koodikannalle.
Esimerkiksi syntaksitarkistukset, yksikkötestit ja ohjelmistojen kääntäminen soveltuvat hyvin automaatioputkien suoritettaviksi, sillä niihin harvoin tarvitaan syötettä käyttäjältä.
Tällä voidaan esimerkiksi varmistua siitä, että muutosten teon yhteydessä ajetaan resurssi-intensiivisiä testejä, joita yksittäinen kehittäjä ei omalla työasemallaan voi toteuttaa.

Tämän työn käsittelemässä tapauksessa automaatioputket toimivat yksittäisten automatisoitujen prosessivaiheiden käynnistäjinä ja kanavoivat informaatiota vaiheista toiseen.
Automaatioputket käynnistetään aina manuaalisesti, mutta tämän jälkeen koko uusiokäyttöprosessi tapahtuu automaattisesti valmistumiseen tai hallittuun virhetilan kohtaamiseen asti.



\subsection{Automatisoidun toteutuksen perusteknologioita}

Automaatioputkissa ajettavat vaiheet ja tehtävät perustuvat suhteellisen yksinkertaisiin perusteknologioihin.
Jokainen tehtävä suoritetaan sitä varten rakennetussa ohjelmistokontissa käyttäen yksinkertaisia skripti- tai konfiguraationhallintakieliä.
Nämä skriptit toteuttavat jonkin selkeästi määritellyn prosessivaiheen siten, että vaiheen alkutilasta voidaan aina tehdä tiettyjä oletuksia, jotka ensiksi aina vahvistetaan.
Tällöin voidaan suurella varmuudella siirtyä itse ohjelmakoodin suorittamiseen.
Perusteknologioille ominaista on, että ne ovat käytössä laajasti myös muissa yhteyksissä.

\subsubsection*{Docker}
Jokainen automatisoitu prosessivaihe suoritetaan sitä varten rakennetussa ohjelmistokontissa.
On keskeistä, että jokainen vaihe voidaan toistaa täysin samassa ajoympäristössä, jotta vikatilanteet ja ohjelmistovirheet on mahdollista toisintaa ja korjata.
Ulkoisten riippuvuuksien vähentämiseksi ohjelmistokontit kootaan itse, ja ne säilötään ja versioidaan konttirekisterissä.

Kontitus on toteutettu OCI-yhteensopivasti Dockeria käyttäen, joka on yleinen työkalu ohjelmistokonttikuvien rakentamiseen, toteuttamiseen ja ajamiseen.
Dockerin perusperiaate on, että jokainen ohjelmistokonttikuva määritellään tiedostopohjaisesti, kuten kuvan \ref{dockerfile} esimerkki osoittaa.
Jokainen rivi tiedostossa vastaa ohjelmistokonttikuvassa yhtä kerrosta, joten niiden järjestyksellä on merkitystä.
Usein muuttuvat tai kehityksen alla olevat toiminnot tulee sijoittaa tiedoston loppuun, mikäli ohjelmistokonttikuvan muodostamisajalla ja -prosessilla on merkitystä.
Docker mahdollistaa myös ohjelmistokonttikuvien muodostamisen kehittäjän omalla työasemalla, jonka avulla on mahdollista testata muutoksia konttikuviin pitkälti lokaalisti.
\cite{oci-myths}




\begin{figure}[h]
    \caption{Esimerkki Dockerin käyttämästä konttikuvatiedostosta}
    \centering
    \vspace*{3mm}
    \begin{tabular}{ |l| }
        \hline
         \\
        \begin{minipage}{0.9\textwidth}
        \VerbatimInput{files/Dockerfile}
        \end{minipage}\\
         \\
        \hline
    \end{tabular}
    \label{dockerfile}
\end{figure}

Yllä kuvassa \ref{dockerfile} määritellään hyvin yksinkertainen ohjelmistokontti, jolla voidaan suorittaa jokin Pythonilla toteutettu ohjelma.
Rivillä 1 määritellään, että ohjelmistokontin pohjana käytetään julkisesti saatavilla olevaa ohjelmistokonttia nimeltä \texttt{python} ja siitä versiota \texttt{3.12}
Rivillä 2 kopioidaan sovelluskansion sisältö ohjelmistokontin nykyhakemistoon, jonka jälkeen rivillä 3 asennetaan tarvittavat riippuvuudet.
Rivi 4 kertoo, mitä ohjelmistokontti tekee käynnistyessään, eli tässä tapauksessa ajaa python-ohjelman kontin sisällä.
\cite{docker-from}
\cite{docker-add}
\cite{docker-run}
\cite{docker-cmd}





























\subsubsection*{Ansible}
Tässä työssä esitellyssä automatisoidussa uusiokäyttöjärjestelmässä Ansiblea käytetään palvelinten konfiguraationhallintaan ja joidenkin työvaiheiden automatisointiin.
Ansible on konfiguraationhallintatyökalu, joka mahdollistaa automaatio-operaatioiden korkeatasoisen abstraktion.
Käytännössä tämä tarkoittaa sitä, ettei käyttäjän tarvitse itse toteuttaa monimutkaisia virheenkäsittelyjä tai etäoperaatioita, vaan pelkkä listaus automaation kohteista ja aiotusta lopputilasta riittää.
Ansiblen käytöllä on kaksi edellytystä: yksiselitteinen inventaario ja lopputilan selkeä määrittely.

Yksiselitteisellä inventaariolla tarkoitetaan tapaa, jolla Ansiblelle kerrotaan sen operaatioiden kohde tai kohteet.
Inventaariossa kohteet jaotellaan ryhmiin, jolloin automaatio-operaatioita voidaan tehokkaasti kohdistaa ja rajata ryhmäkohtaisesti.
Esimerkki inventaariosta on esitetty kuvassa \ref{ansible-inventory}.
\cite{ansible-building-inventories}



\begin{figure}[H]
    \caption{Esimerkki yksinkertaisesta Ansible-inventaariosta}
    \centering
    \vspace*{3mm}
    \begin{tabular}{ |l| }
        \hline
         \\
        \begin{minipage}{0.9\textwidth}
        \VerbatimInput{files/ansible-hosts}
        \end{minipage}\\
         \\
        \hline
    \end{tabular}
    \label{ansible-inventory}
\end{figure}

Automaatio-operaatiot kuvataan Ansiblessa yksinkertaisina ja deklaratiivisina listauksina, YAML-formaatissa.
Tarkoituksena on, että loppukäyttäjä määrittelee halutun lopputilan, jonka jälkeen Ansible huolehtii kohdepalvelimien konfiguroinnista lopputilaa vastaaviksi.
Esimerkki on kuvattuna kuvassa \ref{ansible-playbook}.
Nämä kuvaukset voidaan yhdistää loogisiksi kokonaisuuksiksi, ja niiden onnistumista ja toteumaa voidaan arvioida ajonaikaisesti.
\cite{ansible-playbooks}



\begin{figure}[H]
    \caption{Ansiblella toteutettua konfiguraationhallintaa}
    \centering
    \vspace*{3mm}
    \begin{tabular}{ |l| }
        \hline
         \\
        \begin{minipage}{0.9\textwidth}
        \VerbatimInput{files/playbook.yml}
        \end{minipage}\\
         \\
        \hline
    \end{tabular}
    \label{ansible-playbook}
\end{figure}

Kuvat \ref{ansible-inventory} ja \ref{ansible-playbook} muodostavat kokonaisuuden, jossa kuusi palvelinta on ryhmitelty kahteen ryhmään.
Tämän jälkeen palvelimille lisätään käyttäjiä, mutta ehdollisesti siten, että kaikille palvelimille lisätään käyttäjä \textit{Roope}, mutta vain ryhmälle A \textit{Matti} ja ryhmälle B \textit{Maija}.




\subsubsection*{Python}
Python on laajasti käytetty ja tulkattava ohjelmointikieli, jolla on yksinkertaista toteuttaa skriptejä ja ohjelmia.
Tämän työn kontekstissa pythonia käytetään toteuttamaan toiminnallisuuksia Ansibleen, sekä pienten ohjelmien toteuttamiseen.

Ansiblen toimintalogiikka perustuu aiemmin kuvattujen yksinkertaisten lopputilan kuvausten toteuttamisesta pythonilla kirjoitetuilla moduuleilla.
Näitä moduuleita toimitetaan valmiina Ansible-asennuksen mukana useita, mutta joskus on tarpeellista kirjoittaa omia moduuleita tai korjata virheitä valmiissa toteutuksissa.
Moduulit mahdollistavat tarkan virheenkäsittelyn ja tehokkaan, tarkoitukseen sopivan toteutuksen.
\cite{ansible-modules}
\cite{writing-ansible-modules}

Usein on hyödyllistä kirjoittaa jatkuvasti ajettavia ohjelmakokonaisuuksia, joiden tehtävänä on valvoa palvelinten tilaa.
Tällainen ohjelma voi esimerkiksi valvoa jonkin kansion tiedostojen kokoa ja määrää, tarvittaessa poistaen vanhoja tiedostoja.
Näin voidaan ehkäistä tiedostojärjestelmän täyttyminen ja palvelimen toiminnan estyminen.
Näiden toteuttaminen pythonilla on yksinkertaista, sillä python tarjoaa laajat työkalut käyttöjärjestelmätason operointiin.
\cite{python-os}







\subsubsection*{bash}
Bash on komentotulkki, jolle on mahdollista toteuttaa skriptejä ja ohjelmia käyttöjärjestelmän työkaluja hyödyntäen.
Esimerkki bash-skriptistä on kuvassa \ref{bash-skripti}.


\begin{figure}[H]
    \caption{Esimerkki yksinkertaisesta bash-skriptistä}
    \centering
    \vspace*{3mm}
    \begin{tabular}{ |l| }
        \hline
         \\
        \begin{minipage}{0.9\textwidth}
        \VerbatimInput{files/script.sh}
        \end{minipage}\\
         \\
        \hline
    \end{tabular}
    \label{bash-skripti}
\end{figure}

Skriptissä rivillä 1 määritetään yksiselitteisesti polku komentotulkkiohjelmaan, jolla tiedosto suoritetaan.
Tämän jälkeen skripti laskee kansiossa olevat tiedostot rivillä 4 ja kirjoittaa logitiedostoon ajanhetken, jolloin se ajettiin.

Huomionarvoista on, ettei skripti varsinaisesti implementoi itse mitään toiminnallisuuksia, vaan ainoastaan hyötykäyttää olemassa olevia käyttöjärjestelmätyökaluja.
Näiden työkalujen ulostulo ja paluuarvoja ketjutetaan siten, että toiminnallisuuksia saadaan yhdistettyä.
Esimerkiksi rivillä 4 komento \texttt{ls -a} listaa kaikki tiedostot, jonka jälkeen lista tiedostoista annetaan komennolle \texttt{wc -l} niiden laskemista varten.

Bash-skriptit ovat myös olennainen osa automaatioputkia, esimerkiksi kuvassa \ref{gitlab-ci} riveillä 4, 9, 14, 15 ja 20 listatut komennot ajetaan bashilla.


\newpage


\subsection{Automatisoidun käyttöjärjestelmän asennuksen prosessikuvaus}
Automatisoitu asennustoteutus perustuu automaatioputkiin.
Automaatioputkia on useita, ja ne on esitelty taulukossa \ref{prosessiputket}.
Prosessin alkuvaiheessa palvelin on joko jossain muussa käytössä, tai täysin uusi palvelin.
Prosessin viisin vaihetta huolehtivat siitä, että palvelin on käyttövalmis lähtötilanteesta riippumatta.


\vspace{0.2cm}
\begin{table}[H]
    \caption{Palvelimen asennus- ja uusiokäyttöprosessin viisi karkeaa työvaihetta.}
    \begin{center}
        \begin{tabular}{|l|p{32mm}|p{50mm}|p{40mm}|}
            \hline
            \textbf{Vaihe} & \textbf{Komponentti} & \textbf{Vaiheen vastuut} & \textbf{Käytetyt teknologiat} \\
            \hline
            1 & Palvelimen asennusvalmiuden varmistaminen & Varmistetaan, ettei palvelimella ole aktiivista käyttöä, ja se voidaan asentaa uudelleen. Vaihe toteutetaan myös uusille palvelimille. & Python, rajapintakutsut tietolähteisiin \\
            \hline
            2 & Asennusta edeltävä kirjanpitovaihe & Asennuksen esivalmistelut ja kirjanpitotiedon täydentäminen tarvittaessa. & Python, Ansible\\
            \hline
            3 & Asennuslevykuvan muodostaminen & Luodaan levykuva, josta palvelin voidaan asentaa. Levykuva sisältää verkko- ja tiedostojärjestelmäkonfiguraatiota. & Bash, Ansible \\
            \hline
            4 & Palvelimen käyttöjärjestelmän asentaminen & Palvelin asennetaan levykuvaa käyttäen. & Ansible, etähallintatyökalut \\
            \hline
            5 & Palvelimen konfiguraationhallinta ja testit & Varmistetaan, että palvelimen konfiguraatio vastaa haluttua tilaa & Ansible \\
            \hline
        \end{tabular}

    \end{center}
    \label{prosessiputket}
\end{table}
\vspace{0.2cm}


Jokainen viidestä taulukossa \ref{prosessiputket} kuvatusta vaiheesta pohjautuu pitkälti samoille tässä luvussa kuvatuille perusteknologioille.
Prosessi on jaettu vaiheisiin siten, että jonkin vaiheen epäonnistuessa sitä on helppo yrittää uudelleen aiempien vaiheiden tulosten perusteella.
Toisaalta prosessissa ei myöskään pidä edetä, ellei aiempien vaiheiden onnistumisesta ole suurta varmuutta, sillä etenkin vaiheissa 2 ja 4 tehdään peruuttamattomia asioita, sillä kirjanpitoa muokataan ja palvelimen käyttöjärjestelmä asennetaan uudelleen.

\subsubsection*{Palvelimen asennusvalmiuden varmistaminen}

Asennusvalmiuden varmistamisella pyritään tekemään prosessista häiriötön loppukäyttäjän näkökulmasta.
Tässä vaiheessa palvelimelle ei tehdä minkäänlaisia muutoksia tai muita toimia, vaan palvelimen tilaa ainoastaan verrataan kirjanpitojärjestelmistä löytyvään tietoon.

Prosessilla ei välttämättä ole mitään keinoa tietää, onko palvelimen tila oikea.
Tästä johtuen kaikki kirjanpitojärjestelmiin liittyvät ristiriidat annetaan uusiokäyttö- tai asennusprosessin käynnistäjän tarkastettaviksi.
Käynnistäjän tehtävänä on korjata joko palvelimen tilaa tai kirjanpitojärjestelmien tietoja siten, ettei näiden välillä ole ristiriitaa ja asennusprosessia voidaan jatkaa.




\subsubsection*{Asennusta edeltävä kirjanpitovaihe}

Kun palvelimen asennusvalmiudesta ollaan varmoja, voidaan aloittaa aktiivinen vaihe.
Tässä vaiheessa prosessin käynnistäjän syötteen perusteella tehdään muutoksia kirjanpitojärjestelmiin.
Nämä muutokset pitävät sisällään esimerkiksi palvelimen fyysiseen sijaintiin, verkkoasetuksiin tai nimeen liittyviä toimenpiteitä.

Tämän vaiheen aikana varmistetaan, että kaikki palvelimen asennuksen edellyttämä tieto on päivitetty asianmukaisesti kirjanpitojärjestelmiin.
Vaikka palvelin olisikin asennusvalmis, ei tätä vaihetta voida välttämättä viedä loppuun asti esimerkiksi tilanteessa, jossa palvelimelle ei riitä enää resursseja.
Esimerkiksi palvelimen siirtyessä verkosta toiseen on mahdollista, ettei verkossa ole enää vapaita IP-osoitteita, jonka johdosta palvelin ei voi toimia kyseisessä verkossa.
Tässä vaiheessa kaikki muutokset validoidaan ensin, jonka jälkeen ne vasta päivitetään kirjanpitojärjestelmiin.
Tällä varmistetaan, että vaiheen epäonnistuessa pysyviä muutoksia ei tallenneta ja vaihe voidaan aloittaa alusta.
Kuten aiemmassakin vaiheessa, virheet tuodaan käyttäjän tietoon korjausta ja arviointia varten. 


\subsubsection*{Asennuslevykuvan muodostaminen}

Levykuvan muodostaminen on hyvin mekaaninen prosessi, joka pohjaa vahvasti vaiheessa 2 päivitettyyn kirjanpitotietoon.
Vaiheessa valmistetaan ensin tiedosto, joka mahdollistaa konfiguraationhallinnan asennusprosessin aikana.
Käytännössä kirjanpitotiedon perusteella tähän tiedostoon lisätään verkkoasetukset, palvelimen nimi sekä tiedostojärjestelmän konfiguraatio.

Asetustiedoston luomisen jälkeen se injektoidaan geneeriseen käyttöjärjestelmän asennuslevukuvaan siten, että asennuksen yhteydessä aiemmin mainitut asetukset konfigurodiaan kyseiselle palvelimelle.
Levykuvan luomisen jälkeen se nimetään kuvaavasti ja tallennetaan paikkaan, josta se voidaan myöhemmässä vaiheessa hakea asennusta varten.

\subsubsection*{Palvelimen käyttöjärjestelmän asentaminen}

Palvelimen asentaminen tapahtuu aiemmassa vaiheessa luotua levykuvaa käyttäen.
Tässä vaiheessa yksinkertaisesti otetaan yhteyttä palvelimen etähallintaohjelmistoon, ja ohjeistetaan palvelinta käynnistämään itsensä uudelleen asennuslevykuvan avulla.
Tämän jälkeen palvelin uudelleenkäynnistyy, hakee verkon ylitse asennuslevukuvan ja asentuu uudelleen.

Tämä vaihe on yksinkertaisin, mutta samalla myös kriittisin, sillä palvelimen asennus edellyttää käytännössä kaiken sillä säilytetyn datan ja konfiguraation menettämistä.
Aiemmissa vaiheissa toteutetut toimenpiteet yksinkertaisesti varmistavat, ettei tällä ole merkitystä.

\subsubsection*{Palvelimen konfiguraationhallinta ja testit}

Käyttöjärjeselmän asentamisen jälkeen ollaan tilanteessa, jossa palvelin on pitkälti käyttövalmis.
Palvelimelle on asennettava vielä useita työkaluja ja konfiguraatiota, ja tästä huolehditaan tässä vaiheessa.

Konfiguraation asentamisen jälkeen palvelimelle ajetaan testejä siten, että verkkoyhteydet ja konfiguraatio kyetään validoimaan.
Validoinnin jälkeen palvelin on valmis tuotantokäyttöön.


Tässä luvussa käydään läpi palvelinten perusolemus sekä uusiokäytön konsepti.
Ensimmäiset aliluvut keskittyvät palvelinten perusajatuksen läpikäyntiin sekä konseptin esittelyyn, seuraavat myöhemmät aliluvut kuvaavat uusiokäyttökonseptin ja sen edellytykset.

\subsection{Palvelin konseptina}

Palvelimella yleisesti ottaen tarkoitetaan tietokonetta, joka keskitetysti tarjoilee jonkin asteista palvelua tai dataa muille asiakkaina toimiville tietokoneille.
Palvelin toteutta tehtäväänsä lähtökohtaisesti riippumatta siitä, onko sillä asiakkaita vai ei.
Toisinsanoen asiakkaat eivät määrittele palvelimen toimintaa yksipuolisesti.
Tämän diplomityöntyön kannalta olennaisinta palvelinten konseptissa on niiden korvattavuus: vaikka palvelin on keskitetty resurssi, tulee sen silti olla korvattavissa kohtuullisella tavalla.

Yleisesti ottaen palvelimet ovat diskreettejä yksiköitä, joilla on jokin hyvin määritelty perustehtävä.
Tämä perustehtävä asettaa reunaehdot esimerkiksi sille, miten usein palvelimen voi uudelleenkäynnistää tai miten toimitaan vikatilanteen ilmetessä.
Karkeasti ottaen perustehtävät voidaan jakaa kahteen ryhmään sen perusteella, tuleeko perustehtävää toteuttaa jatkuvasti ilman häiriöitä vai ei.
Jaottelua voi jatkaa sillä perusteella, miten lyhyellä varoitusajalla palvelukatkon voi toteuttaa ja miten pitkä se saa olla.

Palvelin itsessään on konseptina peräisin 1960- ja 1970-luvun vaihteesta, jolloin se määriteltiin osana ARPANET:iä komponentiksi, joka vastaanottaa ja välittää viestejä.
Nykyisin ARPANET:in on korvannut Internet, joka pohjautuu modernimpiin teknologioihin, mutta palvelimen konsepti on silti pääpiirteittäin sama.
Tämä työ keskittyy palvelimiin, joita käytetään osana modernia Internetiä, toisin sanoen aikaan WWW:n julkaisemisen jälkeen.
\cite{rfc-5}
\cite{ARPANET}
\cite{CERN-IT-9304003}

Internetin yleistyminen 1990-luvulla johti palvelinten määrän lähes räjähdysmäiseen kasvuun ensiksi siitä syystä, että erilaisia käyttökohteita syntyi lisää ja toiseksi siitä syystä, että käyttäjien määrä kasvoi merkittävästi läpi koko vuosikymmenen.
Tämä kasvu ei ole sittemmin hidastunut, joten palvelinten käyttö ja hallinta on kehittynyt merkittävästi, koska palvelinten määrän kasvaessa niiden korvattavuudelle ja hallinnalle on erilaiset edellytykset.
Kuten kuvasta \ref{internetinkehitys} nähdään, on kehitys ollut jatkuvasti kasvavaa.

\begin{figure}[h]
    \caption{Internetin käyttäjien määrän kehitys ajan funktiona.\cite{ITU_internet_2023}}
    \begin{center}
        \includegraphics[width=\textwidth]{internet_users.png}
    \end{center}
    \label{internetinkehitys}
\end{figure}



Aluksi palvelimet olivat käytännössä aina yksittäisiä fyysisiä yksiköitä, joiden asennus perustui manuaalisiin operaatioihin sekä fyysisen tilan fasilitointiin.
Niitä käytettiin myös lähtökohtaisesti verkoissa, joissa käyttäjiä oli vähän ja joihin pääsy oli rajattu, joten tietoturvan merkitys pysyi vähäisenä.
Fyysiset palvelimet koostuvat tarkoitusta varten suunnitelluista PC-komponenteista, joten niiden huoltaminen vaatii varaosia ja sitä varten rakennetut fasiliteetit.

Palvelinten määrän ohella teknologia on kehittynyt ja kypsynyt siten, että hyvin usein varsinainen palvelin on virtualisoitu instanssi, eikä sillä ole välttämättä fyysistä sidonnaisuutta lainkaan.
Internet ja yksittäisen palvelimen lähes globaali käyttö asettaa tietoturvavaatimukset uudelle tasolle, sillä jokainen teoreettinenkin haavoittuvuus on käytännössä kenen tahansa käytössä.


Tekninen kehitys ei kuitenkaan ole täysin poistanut tarvetta fyysisille palvelimille, sillä ne ovat edelleen ehdoton toimintaedellytys virtualisoinnille ja muille toiminnoille ja täten ne luovat perustan palvelintekniikan kehitykselle.
Fyysisten palvelinten määrä jatkaa edelleen kasvamistaan osana digiyhteiskunnan kehitystä, eikä tarkkoja ennusteita niiden kasvun hiljenemisestä ole olemassa.
\cite{mckinsey-datacenter}


Palvelun tuottamiseksi palvelimet jakavat resursseja.
Jaettavia resursseja ovat pääsääntöisesti prosessointiaika, keskusmuisti, tallennustila ja verkkokapasiteetti.


\subsection{Palvelinten käyttökohteet}
Palvelimia voidaan käyttää lukemattomiin eri tarkoituksiin, mutta kaikki nämä tarkoitukset voidaan karkeasti ottaen jakaa muutamaan kategoriaan kyseisten palveluiden perusteella.
Toinen jaottelukriteeri on palvelinten yhteistoimintaedellytykset ja -mahdollisuudet, sillä käyttötarkoituksesta riippuen esimerkiksi datan synkronointi palvelinten välillä laiterikkojen vaikutusten ehkäisemiseksi voi olla ehdoton vaatimus.


Palvelinten toimintaa ohjaa, rajoittaa ja mahdollistaa niiden yhteydessä toimivat palvelut ja järjestelmät.
Tällaisia palveluita ovat esimerkiksi verkkotason palomuuri ja reititysratkaisut, sekä palvelimissa käytössä oleva käyttöjärjestelmä.
Lisäksi esimerkiksi pääsynhallinnan mahdollistamiseen voidaan käyttää ulkoisia tietolähteitä, jolloin palvelimet eivät itse pidä kirjaa siitä, keille etäpääsy on myönnetty tai keiltä se on estetty.

Käyttöjärjestelmän tehtävänä on hallinnoida palvelimessa itsessään käynnissä olevia prosesseja ja palveluita.
Käyttöjärjestelmä huolehtii siitä, että tieto tallennetaan oikein ja palvelinten resurssit jaetaan tehokkaasti.
Käyttöjärjestelmään syvennytään tarkemmin myöhemmin tässä työssä.

Palvelinten tietoturvan ja toiminnan kannalta on keskeistä, että pääsynhallinta on mahdollisimman rajattua jokaisella asteella verkkotasosta lähtien.
Tiivistäen, kaiken pitäisi olla oletusarvoisesti estettyä ja ainoastaan rajatuissa tapauksissa voidaan tehdä poikkeuksia pääsyn mahdollistamiseksi.
Esimerkiksi palomuureilla voidaan erikseen estää liikenne kaikkialta, sallien ainoastaan tietyt ulkoiset lähteet.
Samaten verkkoeristys on mahdollista toteuttaa siten, ettei palvelinten verkoissa toimi ylimääräisiä tahoja, jotka saattaisivat muodostua tietoturvaongelmiksi.



\subsection{Erilaiset palvelintyypit}
Palvelimet voidaan karkeasti jaotella kolmeen eri kategoriaan sen perusteella, millaisessa arkkitehtuurissa niitä ajetaan.
Jokaisessa tapauksessa lopputulema on sama, eli palvelin fasilitoi loppukäyttäjälle jonkin palvelun.
Palvelintyypit on esitetty taulukossa \ref{palvelintyypit}.

\vspace{0.2cm}
\begin{table}[H]
    \caption{Palvelintyyppejä}
    \begin{center}
        \begin{tabular}{|l|p{35mm}|p{35mm}|p{35mm}|}
            \hline
             Palvelintyyppi & \textbf{Fyysinen} & \textbf{Virtuaalinen} & \textbf{Funktionaalinen}\\
            \hline
            Lyhyt kuvaus & Palvelinkaappiin asennettu fyysinen laite, jolla ajetaan käyttöjärjestelmää. & Rinnastuu fyysiseen palvelimeen. Käyttöjärjestelmän ja fyysisten resurssien välissä on virtualisointikerros. & Virtuaalinen palvelin, joka käynnistettään tietyssä tilanteessa toteuttamaan jokin hyvin ennalta määritelty operaatio.\\
            \hline
            Hankinta-aika & Päivistä viikkoihin & Lähes välitön & Lähes välitön\\
            \hline
            Korvattavuus & Riippuu hankinta-ajasta & Lähes välitön & - \\
            \hline
            Monistettavuus & Edellyttää laitehankintoja. & Monistettavissa virtualisointikerroksen asettamissa rajoissa. & - \\
            \hline
            Skaalautuvuus & Hidasta & Nopeaa & Nopeaa\\
            \hline
            Liiketoimintamalli & Laitteet ostetaan tai vuokrataan, jonka lisäksi kustennuksia tulee lähinnä sähköstä ja toimintaympäristöstä. & Tyypillisesti virtuaalikoneista maksetaan käytön ja niille tarjottujen resurssien mukaan. & Kulut koostuvat yksittäisistä ajokerroista ja ajopyyntöjen käsittelystä.\\
            \hline
            Käyttökohteita & Suorituskykyä vaativat sovellukset. \newline \newline Esimerkiksi virtualisten ja funktionaalisten palvelinten fasilitointi. \newline \newline Rautapohjaisten tietoturvaominaisuuksien hyödyntäminen.  & Selkeät, hyvin rajatut tehtävät. \newline \newline Hyvin monistuvat käyttötarkoitukset. & Funktionaaliset ja lyhyen ajoajan omaavat asiat. Esimerkiksi jonkin pyynnön käsittely tai jokin harvoin toteutettu operaatio.\\
            \hline
        \end{tabular}

    \end{center}
    \label{palvelintyypit}
\end{table}
\vspace{0.2cm}

Kuten taulukosta \ref{palvelintyypit} nähdään, erilaisten palvelinten toimintaedellytykset ja -vaatimukset eroavat toisistaan merkittävästi.
Esimerkiksi fyysisten palvelinten monistaminen ja korvaaminen edellyttää asennusoperaatioita ja laitehankintoja, siinä missä virtuaaliset palvelimet voidaan korvata triviaalisti ja funktionaalisten palvelinten kohdalla korvattavuus ei ole edes relevanttia.

Virtualisoiduilla palvelimilla voidaan lähtökohtaisesti toteuttaa täysin samat tehtävät kuin fyysisilläkin palvelimilla, ja nykyisin se onkin usein suosituin vaihtoehto ellei jotain erityistä syytä fyysisten palvelinten käyttöön ole.
Virtualisoinnilla mahdollistetaan kiinteiden laitehankintakustannusten pienentäminen ja katkaistaan tehokkaasti palveluiden riippuvuudet yksittäisiin tietokoneisiin.
\cite{9245124}

Virtualisoinnilla on aina jokin reaalinen kustannus resurssienkäytön kannalta, jonka johdosta fyysisten palvelimien käyttö on joskus ainoa vaihtoehto.
Fyysisten palvelinten käyttö mahdollistaa myös esimerkiksi rautapohjaisten salausteknologioiden käytön, joka on usein tehokkaampaa verrattuna ohjelmistollisiin totetuksiin.
\cite{IBM-RAUTA-KRYPTO}

Funktionaalisten palvelimien kohdalla korvattavuus ja monistettavuus ei ole relevanttia, sillä niiden olemassaoloaika on verrattain lyhyt, eivätkä ne yleensä ylläpidä mitään varsinaista tilaa.
Tällaisia tarkoituksia ovat esimerkiksi pilvipalvelutarjoajien funktionaaliset palvelintuotteet.
Funktionaalisten palvelinten toimintaedellytyksiin lukeutuu merkittävä määrä abstraktiotasoja, jotka osaltaan koostuvat fyysisistä ja virtualisoiduista palvelimista.
\cite{AZURE-FUNCTIONS}



\subsection{Palvelinten hallinnointi}
Fyysisten palvelinten käytön ja operoinnin edellytys on niiden kestävä hallinnointi.
Palvelimet sijaitsevat konesaleissa ja niiden sijainnista ja asennuksen yksityiskohdista tulee pitää kirjaa jossain järjestelmässä.
Toimiva kirjanpito mahdollistaa etäkäytön eri tasoilla esimerkiksi palvelimessa olevien hallintalaitteiden avulla tai käyttöjärjestelmätasolla SSH:ta käyttäen.
Etähallinta parantaa palvelinten fyysisen turvallisuuden aspektia, sillä fyysinen pääsy voidaan rajata vain palvelinten fyysisiä asennuksia tekeville henkilöille.
Etähallintaan syvennytään tämän työn myöhemmässä luvussa.
Fyysinen turvallisuus on tärkeää, jotta voidaan estää esimerkiksi laitevarkaudet ja tahallinen haitanteko laitteet väkivaltaisesti rikkoen.
Yleisellä tasolla yksittäisen konesalin arkkitehtuuri on kuvattu kuvassa \ref{arkkitehtuurikuva}.

Palvelinten sijoittelu datakeskuksissa vaatii suunnittelua ja monitorointia, sillä palvelimet tuottavat paljon lämpöä.
Tästä syystä palvelinten tulee sijaita riittävän väljästi, jotta ilma pääsee kiertämään ja vältetään ylikuumenemisesta johtuvia laitevikoja.
\cite{8757279}

Virtuaalipalvelinten suhteen vastaavaa suunnittelua ei tarvitse tehdä, mutta kirjanpidolliset aspektit etähallinnan mahdollistajana pätevät myös niihin.
Etähallinta on virtuaalipalvelinten kohdalla dynaamisempaa, sillä virtualisointikerros tarjoaa tähän usein tehokkaat työkalut, jotka toimivat käyttöjärjestelmästä irrallaan.


Olennaista on että tässä luvussa kuvataan yleisellä tasolla, miten usean datakeskuksen muodostama palvelinarkkitehtuuri toimii ja millaiset lainalaisuudet siinä vallitsevat.

\vspace{0.2cm}
\begin{figure}[H]
    \caption{Yleinen kuvaus palvelinsalin arkkitehtuurista. \cite{dc-arkkitehtuuri}}
    \begin{center}
        \includegraphics[width=\textwidth]{dc_arkkitehtuuri.png}
    \end{center}
    \label{arkkitehtuurikuva}
\end{figure}
\vspace{0.2cm}

Kuvassa \ref{arkkitehtuurikuva} on kuvattu palvelinsalisuunnittelun peruskomponentit ja -periaatteet.
Jokainen verkkotason komponentti kahdennetaan, ja palvelimet jaotellaan loogisiksi kokonaisuuksiksi palvelinkehikoihin.
Jokaisessa palvelinkehikossa on oma kytkimensä, jonka kautta liikenne reititetään pää- ja koontikytkimiin.

Usein palvelinkehikoiden välillä voidaan toteuttaa myös kahdennusta siten, että yksittäisen palvelinkehikon kautta kulkee kahden vierekkäisen kehikon liikenne.
Tällöin mikään yksittäinen verkkolaite ei voi vikaantuessaan keskeyttää palvelinten toimintaa.


\subsection{Kapasiteettihallinta}
Jotta palvelinten resursseja voidaan jakaa tehokkaasti, tulee niiden käyttöä seurata ja tarvittaessa rajoittaa.
Käytön seuraamista ja rajoittamista, sekä siitä syntyviä toimia kutsutaan kapasiteettihallinnaksi.
Kapasiteettihallinta on prosessi jossa palvelimia otetaan käyttöön, poistetaan käytöstä tai uusiokäytetään.
Prosessin tavoitteena on saada maksimaalinen määrä resursseja käytettyä siten, että toiminnan luotettavuus ei vaarannu.
Resurssit voidaan jakaa tässä yhteydessä konesali-, verkko- ja palvelinkohtaisiin resursseihin.
\cite{kapasiteetti-blogi-postaus}



Kapasiteettihallinnan tavoitteena on saavuttaa tietty käyttöaste.
Käyttöasteella tarkoitetaan prosentuaalista resurssien käyttöä palvelimilla.
Käyttöasteen ollessa korkea, voidaan minimoida palvelinoperaatioista syntyvät kiinteät kulut.
Käyttöaste on myös hyödyllinen työkalu toiminnan tehokkuuden tarkasteluun, esimerkiksi käyttöasteen ollessa matala, voidaan verrata matalan käyttöasteen omaa ratkaisua ulkoistettuun palveluun.
Ulkoistetun palvelun kustannusksista voidaan johtaa konesalipohjaisen toteutuksen käyttöasteelle pienin hyväksyttävä arvo.

\vspace{0.2cm}
\begin{figure}[H]
    \caption{Ulkoistetun ja itse konesalissa toteutetun palvelun kustannusvertailu esimerkkiluontoisesti}
    \begin{center}
        \includegraphics[width=\textwidth]{puhdas_rajakustannuskuva.png}
    \end{center}
    \label{rajakustannuskuva}
\end{figure}
\vspace{0.2cm}

Konesalissa toteutetun ratkaisun kiinteät kustannukset ovat aluksi suuret, kun taas ulkoistetussa palvelussa yleensä kustannukset ovat käyttöperusteisia.
Näiden kustannusten kehitys on esitetty kuvassa \ref{rajakustannuskuva}.
Käyttäjien ja käyttöasteen kasvaessa konesalipohjaisessa toteutuksessa yksittäisen käyttäjän kustannus pienenee.
Käytännössä tämä tarkoittaa, että käyttöäasteen parantaminen heijastuu suoraan palvelun toteutus- ja ylläpitokustannuksiin.
Kuvassa \ref{rajakustannuskuva} kustannusten kuvaajat leikkaavat noin 550 asiakkaan kohdalla, jonka jälkeen konesalitoteutus on taloudellisesti kannattava.


\subsection{Palvelinten elinkaari}

\vspace{0.2cm}
\begin{figure}[h]
    \caption{Yksittäisen palvelimen tyypillinen elinkaari}
    \begin{center}
        \includegraphics[width=\textwidth]{tyypillinen_elinkaari_2.png}
    \end{center}
    \label{palvelinelinkaarivaiheet}
\end{figure}
\vspace{0.2cm}

Palvelinten elinkaareen vaikuttaa merkittävästi palvelimen tyyppi ja käyttötarkoitus.
Elinkaari pitää sisällään kaikki vaiheet palvelimen hankinnasta sen alasajoon, joskin tässä työssä jätetään huomiotta vaiheet, jotka liittyvät esimerkiksi palvelinten valmistamiseen tai etukäteisvarastointiin.
Elinkaarivaiheita on kuvattu kuvassa \ref{palvelinelinkaarivaiheet}.

Palvelimen elinkaari voidaan karkeasti ottaen jakaa useaan eri vaiheeseen, joiden väliset tilasiirtymät puolestaan ovat palvelimen elinkaarioperaatioita.
Näistä operaatioista osa on monistettavia, ja palvelinten uusiokäytön perusajatus onkin juuri näiden tilasiirtymien käyttäminen kapasiteettihallinnan ja palvelimen rajoitteiden määrittämällä tavalla.
Kuvassa on kuvattu vaihe, jonka aikana uusiokäyttöä tehdään ja se, miten palvelimen tilaa vaihdetaan useaan kertaan samalla tapaa.

Elinkaarihallinta on tärkeä osa kestäviä konesalioperaatioita, sillä palvelimet ja niiden komponentit väistämättä vikaantuvat ja vanhenevat.
Näitä voidaan toki huoltaa takuuehtojen puitteissa, mutta huollon tarve ja takuuehtojen voimassaolo asettavat tiettyjä rajoitteita palvelinten käytölle.
Olennainen osa elinkaarihallintaa on myös teknisen kehityksen huomioiminen.
sein uudet ominaisuudet ja tekninen kehitys luovat tarpeen hankkia uusia palvelimia, jolloin konesalitilan rajoitteiden vuoksi vanhoista palvelimista on hankkiuduttava eroon.

Palvelinten ja palvelinkomponenttien vikaantumista voidaan kuvata ja mitata keskimääräisellä vikaantumisvälillä (MTBF) ja keskimäärisellä vikaantumisajalla (MTTF). 
Aikavälien keskiarvoistamine on tärkeää, sillä komponenttien vikaantuminen on usein jakautunut kuvan \ref{kylpyamme} esittämällä tavalla.

\vspace{0.2cm}
\begin{figure}[h]
    \caption{Palvelinten vikaantumistodennäköisyys ajan funktiona \cite{kylpyamme}}
    \begin{center}
        \includegraphics[width=\textwidth]{kylpyamme.png}
    \end{center}
    \label{kylpyamme}
\end{figure}
\vspace{0.2cm}

Elinkaarivaiheen vikatilanteet voidaan kuvan \ref{kylpyamme} osoittamalla tavalla jakaa kolmeen kategoriaan, valmistusvirheisiin, satunnaisiin vikatilanteisiin ja kulumisesta johtuviin vikatilanteisiin.
Valmistusvirheiden ilmenemistä kuvataan kuvaajassa punaisella, satunnaisia virheitä vihreällä ja kulumisesta johtuvia virheitä keltaisella.
Sininen kuvaaja summaa kaikki nämä kolme kuvaajaa yhdeksi ja siitä nähdään, että virheiden ja vikojen ilmeneminen on todennäköisimmillään palvelimen elinkaaren alku- ja loppupäässä.
\cite{1179819}


Valmistusvirheiden tai huonon laadun aiheuttamat vikatilanteet havaitaan usein varhain ja ne ovat verrattain selkeitä, sillä niiden esiintyminen on ajallisesti hyvin rajoittunutta elinkaaren alkupäähän.
Näiden korjaaminen on usein myös täysin laitetakuun piirissä ja täten hyvin selkeää.

Satunnaiset virheet johtuvat usein erilaisten vikatilanteiden yhteisvaikutuksesta tai ulkoisista tekijöistä.
Huomionarvoista on, ettei niiden minimoimiseksi voida aina välttämättä tehdä mitään merkityksellisiä toimia, sillä satunnaisen luonteensa takia niitä on hankala ennakoida.

Kulumisesta johtuvat vikatilanteet voidaan ennakoida laitteen takuun ja muiden laitteiden käyttöiän perusteella, jolloin pitkän vakaan, viattoman aikajänteen loppuessa palvelimen voidaan olettaa lähestyvän elinkaarensa loppupäätä.



\subsection{Uusiokäyttö konseptina}
Palvelinten uusiokäytöllä tarkoitetaan prosessia, joka hallitusti muuttaa palvelimen käyttötarkoitusta.
Käyttötarkoituksen muutos voi johtua karkeasti jaoteltuna kahdesta syystä; joko siitä, että palvelin ei enää sovi tiettyyn tarkoitukseen tai siitä, että käyttötarkoitus itsessään lakkaa olemasta tai tarve pienenee.
Toisin sanoen tarve uusiokäytölle voi olla joko sisäinen tai ulkoinen.
Molemmissa tapauksissa on todennäköistä, että palvelin itse voi vielä toteuttaa jotain tehtävää, esimerkiksi testi- tai kehityskäytössä.
Uusiokäyttämällä palvelimia tällä tavalla saavutetaan kustannussäästöjä ja vähennetään tarvetta hankkia uusia palvelimia, sillä yksittäisen palvelimen elinkaari voidaan toistaa useaan kertaan.

Palvelinten itsensä kannalta ei ole merkitystä sillä, uusiokäytetäänkö palvelin vai ajetaanko se alas.
Toisaalta sama pätee myös palvelinten ulkopuolisiin kirjanpitojärjestelmiin ja muihin resursseihin.
Palvelinten käyttöhistorialla tulisi olla merkitystä ainoastaan silloin kun päätös uusiokäyttösyklin katkaisemisesta tehdään.



Uusiokäytön tarkat edellytykset vaihtelevat toimintaympäristöstä riippuen.
Yleisesti ottaen uusiokäytön perusedellytykset ovat kuitenkin yhtenevät riippumatta toimintaympäristöstä.

Ensimmäisenä perusedellytyksenä voidaan pitää kykyä irrottaa palvelimen käyttötarkoitus ja identiteetti toisistaan.
Palvelimen tuottaman palvelun käyttäjien kannalta tulisi olla merkityksetöntä, mikä yksittäinen palvelin palvelun tuottaa.
Tarvittaessa palvelu on siis kyettävä siirtämään toiselle palvelimelle ennen uusiokäyttöä.
Toinen perusedellytys on, ettei uusiokäytölle tulisi olla operatiivisesta toiminnasta tai muista järjestelmistä juontuvia esteitä.
Toisin sanoen, palvelin tulee voida ottaa pois aktiivisesta käytöstä ennen uusiokäyttöä.
Ensimmäinen ja toinen perusedellytys tukevat toisiaan.
Niiden toteutumisen ei tarvitse olla saumatonta tai välitöntä.



\subsection{Uusiokäyttöprosessin vaiheet}

Lähtökohtaisesti uusiokäyttöä prosessina ei haluta eriyttää muista palvelinoperaatioista, kuten palvelimen käyttöönotosta tai käytöstä poistamisesta.
Kuten kuva \ref{palvelinelinkaarivaiheet} osoittaa, palvelimen elinkaarihallinta ja uusiokäyttö koostuvat samoista prosesseista ja prosessivaiheista.


Jotta voidaan varmistua siitä, että uusiokäytetty palvelin voidaan rinnastaa uuteen palvelimeen, on uusiokäyttöprosessin syytä olla pitkälle automatisoitu.
Käytöstä poistaminen on prosessi, jonka osana palvelimen toiminnot ja palvelut ajetaan alas hallitussa järjestyksessä.
Olennaisena osana prosessia palvelimen varaamat resurssit myös vapautetaan ja sillä sijaitsevat tiedot poistetaan tarpeellisissa määrin.
Palvelimen käytöstä poistaminen ei välttämättä ole lopullista, sillä käytöstä poistamista saattaa seurata palvelimen käyttöönotto johonkin toiseen tarkoitukseen.
Mikäli palvelin poistuu käytöstä pysyvästi, on osana tätä prosessia myös palvelimen fyysinen poisto ja elektroniikan kierrättäminen.

Palvelimen käyttöönotto on prosessi, jonka avulla palvelin saadaan hyötykäyttöön.
Hyötykäyttö tarkoittaa sitä, että palvelin kykenee suoriutumaan sille määrätyistä tehtävistä.



\subsection{Automaatisoitu uusiokäyttö}
Pääpiirteittän merkityksellistä on automatisoida tietojen poistaminen, päivittäminen ja resurssien varaukset.
Järjestelmän on myös kyettävä selviämään virhetilanteista riittävän autonomisesti, esimerkiksi antamalla järkevä virheilmoitus ja keskeyttämällä prosessi hallitusti.


\subsubsection*{Automaation määritelmä}
Automaatiossa on kyse tunnetun, ihmisten suorittaman prosessin läpikäymisestä koneavusteisesti.
Prosessista tunnistetaan yksittäisiä ihmisen suorittamia kohtia, joissa on taloudellisesti järkevää korvata ihminen.
Tällaisen taloudellisen hyödyn saattaa tuoda esimerkiksi korkean virhemahdollisuuden laskeminen tai prosessin selkeä nopeutuminen.
\cite{/content/paper/5jlz9h56dvq7-en}
\cite{Impact-of-Automation-on-Employment}


\subsubsection*{Autonomian määritelmä}
Autonomia, eli omatoimisuus, tarkoittaa tässä yhteydessä prosessin kykyä sopeutua erilaisiin tilanteisiin ja ulkoisiin muutoksiin.
Esimerkki ulkoisesta muutoksesta on tilanne, jossa jokin rajapinta palauttaa tuntemattoman vastauksen. 
Tällaisessa tilanteessa on kyettävä reagoimaan tuntemattomaan vastaukseen siten, ettei prosessin lopputulos vaarannu merkittävästi.
Reagoinnin ei välttämättä tule olla monimutkaista, usein riittää että prosessin suoritus keskeytyy hallitusti.
\cite{1362627}
\cite{python-exception}









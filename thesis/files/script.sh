#!/bin/bash

echo "Counting files and directories"
ls -a | wc -l

echo "Output date to log file"
echo -n "Counted files at" >> script.log
date >> script.log
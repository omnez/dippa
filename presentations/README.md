# Aalto University beamer themes

This project contains the Aalto Beamer theme files. 

In addition you need to install the `aaltologo` package.

This material is from 2011, so it would be surprising if it worked out of the
box :-)

Current maintainer martin.vermeer@aalto.fi
---

Jussi Pekonen (2011):

Davis Sorenson (2018):

Ville Klar (2019):


## How to use the official Aalto University Beamer themes

The required Beamer theme files are attached. You also need the aaltologo
package (version ≥ 1.1.2) which you can download from the
[Aalto-LaTeX git repository](https://version.aalto.fi/gitlab/latex/aaltologo).

You need to give the document preamble as

````
\documentclass[<class options>]{beamer}
% Include the packages you want to use

%% Required if you are writing in other language than English 
\usepackage[<language option>]{babel}

% More packages...

% Now set the theme \usetheme[<theme options>]{Aalto}

%% Document contents
\begin{document}
% ...
\end{document}
````

Two examples are provided, a basic one and one using the parts functionality of Beamer. 
The examples are called *example.tex* and *example_parts.tex* respectively.
You can build the examples with pdflatex. 
````
pdflatex example.tex
````

If you are preparing a presentation in other language than English it is
recommended that you use the babel package that sets the correct hyphenation
patterns and translates some automatic texts.

There are several theme options available, and they are listed below. If no
options are given, the following options are executed as the default options:
'default' and 'colors'.

### Options for the overall theme

- `default`: as the name states, the default theme
- `sidebar`: a theme that has the table of contents (of the current part) in the
	left sidebar - outline: an alias for option 'sidebar'
- `invariant`: the Aalto University logo remains fixed throughout one part of the
  presentation

### Options for the logo

- `school={<SCHOOL NAME>}`: adds the school name to the Aalto University logo,
	see the documentation of the aaltologo package for the available options
- `institute={<INSTITUTE NAME>}`: adds the institute name to the Aalto University
	logo, see the documentation of the aaltologo package for the available options

### Theme specific options

- `default` theme:
  - `showpartcontents`: shows the table of contents of the newly started part
    automatically
  - `hidesubsections`: hides the subsections from the table of contents of the
    newly started part
- `sidebar` theme:
  - `hideothersubsections`: hides the subsections of other sections from the
    sidebar table of contents
  - `hideallsubsections`: hides all subsections from the sidebar table of contents

Color themes:
- `colors`: uses the colors of Aalto University, the Aalto University logo has
  either a red, blue, or yellow mark
- `blackandwhite`: uses only black and white, the Aalto University logo has a
  black mark
- `grayscale`: uses the gray scale version of the Aalto University gray, black and
  white, the Aalto University logo has a black mark

## Local installation of the slide template

In order to use the template you can either place the .sty files in your presentation directory or install them locally.
To install beamer class locally you need place them in your $TEXMFHOME directory.
To find out the location of $TEXMFHOME run
```
kpsewhich -var-value TEXMFHOME
```

The default $TEXMFHOME folder is usually visible but you can configure it with the $TEXMFHOME environment variable (place in bashrc or equivalent to make persistent)
```
export TEXMFHOME=$HOME/.texmf
```
The .sty files need to be in $TEXMFHOME/texmf/tex/latex/local
```
git clone git@version.aalto.fi:latex/aaltobeamer.git $TEXMFHOME/tex/latex/local
```



Tämä luku käsittelee aiemmissa luvuissa määritellyn prosessin suorittamista manuaalisesti ja automaattisesti.
Luvussa käydään läpi tilasiirtymät ja käsitellään edellytykset niiden automatisoinnille ja manuaaliselle suorittamiselle.
Jokaiselle tilasiirtymälle annetaan arvio virhealttiudesta, käytetystä ajasta ja skaalautuvuudesta manuaalisen prosessin tapauksessa, sekä analysoidaan automaation hyötyä näiden arvioiden parantamisessa.

\subsection{Automaatio tilasiirtymissä}

Tässä aliluvussa keskitytään analysoimaan seitsemää luvussa 4, taulukossa \ref{tilasiirtymät} määriteltyä tilasiirtymää.
Luvussa käsitellään jokainen tilasiirtymä yleisellä tasolla, ja esitetään yksinkertainen analyysi automaation keskeisimmistä hyödyistä.
Jokaiselle työvaiheen ulottuvuudelle annetaan arvio seuraavalla asteikolla:
\begin{itemize}
    \item \textbf{Olematon} - automaation hyöty on kyseenalainen tai se on vaikea toteuttaa
    \item \textbf{Pieni} - automaatiosta on hyötyä, mutta se on pieni tai vaikea toteuttaa
    \item \textbf{Kohtalainen} - automaation hyöty on selkeä
    \item \textbf{Suuri} - automaatio on ehdoton edellytys kyseisessä tilanteessa
\end{itemize}

\subsubsection*{Hankinta $\rightarrow$ Testit}

\vspace{0.2cm}
\begin{table}[H]
        \begin{tabular}{|l|p{60mm}|}
            \hline
            \textbf{Osa-alue} & \textbf{Automaation hyöty}  \\
            \hline
            \textbf{Aikainvestointi} & Pieni \\
            \hline
            \textbf{Virhealttius} & Suuri \\
            \hline
            \textbf{Skaalautuvuus} & Kohtalainen \\
            \hline
        \end{tabular}
\end{table}
\vspace{0.2cm}

Palvelimen komponenttien testaaminen vaihtelee tilanteesta riippuen, mutta tilanne on helppo yleistää uuden palvelimen tapauksessa.
Testien suorittaminen kestää usein useita tunteja tai päiviä ja täten automaation on haastavaa nopeuttaa prosessia merkittävästi.
Suurin hyöty saadaan siis virhealttiuden ja skaalautuvuuden saralla, sillä automaatiolla voidaan esimerkiksi varmistaa, että palvelimille ajetaan tarkoituksenmukaiset testit aina samalla tavalla.
Ihmiskomponentin tehtäväksi jää käynnistää testiautomaatio sen sijaan, että testejä ajetaan yksittäin.
Automaatio voi myös huolehtia tulosten analysoinnista ja tallentamisesta.

\subsubsection*{Testit $\rightarrow$ Käyttövalmius}

\vspace{0.2cm}
\begin{table}[H]
        \begin{tabular}{|l|p{60mm}|}
            \hline
            \textbf{Osa-alue} & \textbf{Automaation hyöty}  \\
            \hline
            \textbf{Aikainvestointi} & Kohtalainen \\
            \hline
            \textbf{Virhealttius} & Suuri \\
            \hline
            \textbf{Skaalautuvuus} & Kohtalainen \\
            \hline
        \end{tabular}
\end{table}
\vspace{0.2cm} 

Palvelimen läpäistessä testit, on tärkeää saada se käyttövalmiiksi mahdollisimman suurella varmuudella.
Tässä automaatiosta on suuri hyöty, sillä sen avulla voidaan poistaa prosessista ihmiskomponentti ja täten saada prosessista yhteneväinen suorituskertojen välillä.
Ajan säästäminen ei ole erityisen ilmeistä automaation avulla, joskin on olennaista että palvelimen aika tilasiirtymässä minimoidaan kustannustehokkuuden maksimoimiseksi.
Palvelimet ovat tuottavia yksiköitä vasta silloin, kun ne suorittavat tuotantokuormia.

\subsubsection*{Testit $\rightarrow$ Poisto} 

\vspace{0.2cm}
\begin{table}[H]
        \begin{tabular}{|l|p{60mm}|}
            \hline
            \textbf{Osa-alue} & \textbf{Automaation hyöty}  \\
            \hline
            \textbf{Aikainvestointi} & Olematon \\
            \hline
            \textbf{Virhealttius} & Kohtalainen \\
            \hline
            \textbf{Skaalautuvuus} & Olematon \\
            \hline
        \end{tabular}
\end{table}
\vspace{0.2cm}

Tilanteessa, jossa palvelin poistetaan testien jälkeen käytöstä, automaation hyöty on pieni.
Tämä johtuu siitä, että tilanne on harvinainen ja usein liittyy vikatilanteisiin.
Tästä syystä automaatio on haastavaa toteuttaa kyseiselle tilasiirtymälle.
Merkittävin hyöty automaatiosta on kirjanpitojärjestelmien päivityksessä.

\subsubsection*{Käyttövalmius $\rightarrow$ Tuotantokäyttö/Testikäyttö} 

\vspace{0.2cm}
\begin{table}[H]
        \begin{tabular}{|l|p{60mm}|}
            \hline
            \textbf{Osa-alue} & \textbf{Automaation hyöty}  \\
            \hline
            \textbf{Aikainvestointi} & Suuri \\
            \hline
            \textbf{Virhealttius} & Suuri \\
            \hline
            \textbf{Skaalautuvuus} & Suuri \\
            \hline
        \end{tabular}
\end{table}
\vspace{0.2cm}

Palvelinten tehokas siirtyminen tuotantokäyttöön on tärkeää kustannustehokkuuden kannalta.
Koska palvelimia siirretään käyttötarkoituksesta toiseen usealla eri tavalla samaan aikaan, kuin uusia palvelimia otetaan käyttöön, ei ihmiskäyttäjän voi olettaa kykenevän tekemään operaatioita manuaalisesti.
Tästä johtuen automaation hyöty virheiden minimoinnissa on keskeistä.

Lisäksi aika-aspektin minimointi ja tilasiirtymien rinnakkaistaminen hyötyvät automaatiosta merkittävästi.
Koska automaattinen toteutus kykenee hakemaan tarvittavat tiedot konfiguraatiosta itsenäisesti, ei ole varsinaista syytä olla suorittamatta käyttöönottoa rinnakkaistettuna.

\subsubsection*{Käyttövalmius $\rightarrow$ Poisto}

\vspace{0.2cm}
\begin{table}[H]
        \begin{tabular}{|l|p{60mm}|}
            \hline
            \textbf{Osa-alue} & \textbf{Automaation hyöty}  \\
            \hline
            \textbf{Aikainvestointi} & Pieni \\
            \hline
            \textbf{Virhealttius} & Kohtalainen \\
            \hline
            \textbf{Skaalautuvuus} & Pieni \\
            \hline
        \end{tabular}
\end{table}
\vspace{0.2cm}

Kuten testien ja poiston välisen tilasiirtymän kohtalla, tämä tilasiirtymä on harvoin kiireinen.
Olennaisinta on varmistua siitä, että kirjanpitojärjestelmät päivitetään automaation avulla oikein.

\subsubsection*{Tuotantokäyttö/Testikäyttö $\rightarrow$ Testit} 

\vspace{0.2cm}
\begin{table}[H]
        \begin{tabular}{|l|p{60mm}|}
            \hline
            \textbf{Osa-alue} & \textbf{Automaation hyöty}  \\
            \hline
            \textbf{Aikainvestointi} & Suuri \\
            \hline
            \textbf{Virhealttius} & Suuri \\
            \hline
            \textbf{Skaalautuvuus} & Olematon \\
            \hline
        \end{tabular}
\end{table}
\vspace{0.2cm}

Tilasiirtymä tuotantokäytöstä testeihin sisältää usein vikatilan tai epäilyn vikatilasta.
Vikatilanteen sattuessa on keskeistä, että palvelin poistetaan tuotantokäytöstä pikimmiten ja siten, että voidaan minimoida tästä palvelun laadulle aiheutuva impakti.

Automaatio on tehokas työkalu evakuoimaan palvelimen palvelut muualle ja varmistamaan, että se voidaan ajaa alas ilman häiriötä kyseisille palveluille.
Vikatilanteet ovat hyvin harvoin samanaikaisia, joten automaation arvo skaalautuvuudelle on pieni tai lähes olematon.


\subsubsection*{Tuotantokäyttö/Testikäyttö $\rightarrow$ Käyttövalmius}

\vspace{0.2cm}
\begin{table}[H]
        \begin{tabular}{|l|p{60mm}|}
            \hline
            \textbf{Osa-alue} & \textbf{Automaation hyöty}  \\
            \hline
            \textbf{Aikainvestointi} & Pieni \\
            \hline
            \textbf{Virhealttius} & Suuri \\
            \hline
            \textbf{Skaalautuvuus} & Pieni \\
            \hline
        \end{tabular}
\end{table}
\vspace{0.2cm}

Tässä tilasiirtymässä on kyse palvelimen hallitusta alasajosta, jotta sen käyttökohdetta voidaan vaihtaa.
Ylivoimaisesti merkityksellisintä tässä tilasiirtymässä on huolehtia alasajon tapahtumisesta siten, ettei palveluille ja asiakkaille aiheudu tästä häiriötä tai muita havaittavia vaikutuksia.
Alasajoa harvoin edellytetään nopeasti tai usealle palvelimelle samanaikaisesti, joten automaation hyöty näissä tilanteissa on pieni, joskin merkityksellinen.

\subsection{Automaation yleiset hyödyt}

Tilasiirtymien analyysista nähdään, että automaation hyöty on suurimmillaan lähes jokaisessa tapauksessa tiedon oikeellisuuden varmistamisessa ja tilasiirtymän virhealttiuden minimoinnissa.
Automaatio mahdollistaa sen, että ihmiskäyttäjän tehtävänä on päättää, milloin automaattinen prosessi käynnistetään ja mille kohteelle.
Automaatio huolehtii siitä, että prosessi suoritetaan oikein ja ilman tarpeettomia häiriöitä palvelunlaadulle.

Automaation voidaan yleisesti ottaen nähdä standardoivan prosesseja ja mahdollistavan niiden selkeän määrittelyn.
Manuaalisissa prosesseissa on usein tilanteita, joissa osa työvaiheista on määrittelemättömiä tai huonosti määriteltyjä, joka taas antaa tilaa ihmisten tekemille tahattomille tulkinnoille ja täten lisää virhealttiutta.
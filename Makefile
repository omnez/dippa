
all:
	make -C thesis
	make -C presentations

tectonic:
	make -C thesis tectonic
	make -C presentations tectonic

clean:
	make -C thesis clean
	make -C presentations clean

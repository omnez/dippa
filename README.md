# Instructions

Install [tectonic](https://tectonic-typesetting.github.io/en-US/) or [texlive](https://wiki.debian.org/Latex) from your distributions repositories.
The Makefile contained within will try to compile the document by using `lualatex` by default.
If you want to use `tectonic`, run `make tectonic`
